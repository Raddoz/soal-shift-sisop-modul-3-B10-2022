#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/stat.h>

#define PORT 8080
pthread_t tid[2];
char buffer[1024] = {0};
int status = 1;
int sock = 0;

void send_file(FILE *fp, int sockfd){
    int n;
    char data[1024] = {0};
 
    while(fgets(data, 1024, fp) != NULL) {
        if (send(sockfd, data, sizeof(data), 0) == -1) {
            perror("[-]Error in sending file.");
            exit(1);
        }
        bzero(data, 1024);
    }
    send(sockfd, "stop signal", sizeof("stop signal"), 0);
}

void write_file(int sockfd, char *filename){
    int n;
    FILE *fp;
    char buffer[1024];
 
    fp = fopen(filename, "w");
    // bzero(buffer, 1024);
    printf("here\n");
    while (1) {
        n = recv(sockfd, buffer, 1024, 0);
        if (n != 1024){
            break;
            return;
        }
        fprintf(fp, "%s", buffer);
        printf("%s\n", buffer);
        bzero(buffer, 1024);
    }
    fclose(fp);
    return;
}

int readPause = 0;

void addProblem() {
    readPause = 1;
    char judul[1024];
    scanf(" %[^\n]", judul);
    // printf("%s\n", judul);
    send(sock, judul, strlen(judul), 0);
    read(sock, buffer, 1024);
    readPause = 0;
    if(!strcmp(buffer, "false")) {
        printf("Judul Problem already used\n");
        return;
    }
    

    char filepath[1024];
    FILE* fp;
    scanf(" %[^\n]", filepath);
    printf("%s\n", filepath);
    fp = fopen(filepath, "r");
    if(fp == NULL)
        printf("Failed to send file\n");
    send_file(fp, sock);
    fclose(fp);

    scanf(" %[^\n]", filepath);
    fp = fopen(filepath, "r");
    send_file(fp, sock);
    fclose(fp);

    scanf(" %[^\n]", filepath);
    fp = fopen(filepath, "r");
    send_file(fp, sock);
    fclose(fp);
}

void downloadProblem() {
    
    char judul[1024];
    memset(judul,0,strlen(judul));
    scanf(" %[^\n]", judul);
    // printf("%s\n", judul);
    send(sock, judul, strlen(judul), 0);
    read(sock, buffer, 1024);
    // printf("%s\n", buffer);
    if(!strcmp(buffer, "false")) {
        readPause = 0;
        printf("Judul Problem not found\n");
        return;
    }
    mkdir(judul, 0777);
    printf("chdir %d\n", chdir(judul));
    write_file(sock, "description.txt");
    write_file(sock, "input.txt");
    readPause = 0;
    chdir("..");
    printf("Problem downloaded\n");
}

void submitProblem() {
    char judul[1024];
    scanf(" %[^\n]", judul);
    // printf("%s\n", judul);
    send(sock, judul, strlen(judul), 0);
    read(sock, buffer, 1024);
    readPause = 0;
    if(!strcmp(buffer, "false")) {
        printf("Judul Problem already used\n");
        return;
    }

    char filepath[1024];
    FILE* fp;
    scanf(" %[^\n]", filepath);
    printf("%s\n", filepath);
    fp = fopen(filepath, "r");
    if(fp == NULL)
        printf("Failed to send file\n");
    send_file(fp, sock);
    fclose(fp);

    read(sock, buffer, 1024);
    readPause = 0;
    printf("%s\n", buffer);
}

void* readandsend() {
    pthread_t id = pthread_self();
    int valread;
    if(pthread_equal(id, tid[0])) {
        while(read(sock, buffer, 1024)) {
            printf("%s\n",buffer);
            memset(buffer,0,strlen(buffer));
            while(readPause)
                sleep(1);
        }
        status = 0;
    }
    else if(pthread_equal(id, tid[1])) {
        char input[1024];
        while(status) {
            scanf(" %s", input);
            // gets(input);
            // printf("masuk %s\n", input);
            send(sock, input, strlen(input), 0);
            if(!strcmp(input, "add"))
                addProblem();
            else if(!strcmp(input, "download")) {
                readPause = 1;
                downloadProblem();
            }
            else if(!strcmp(input, "submit")) {
                readPause = 1;
                submitProblem();
            }
            // memset(input,0,strlen(input));
        }
    }
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    int i;
    for(i = 0; i < 2; i++) {
        int err = pthread_create(&(tid[i]), NULL, &readandsend, NULL);
        // if(err!=0)
        //     printf("\n can't create thread : [%s]",strerror(err));
        // else
        //     printf("\n create thread success\n");
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    return 0;
}

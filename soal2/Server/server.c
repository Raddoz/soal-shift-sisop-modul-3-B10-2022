#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/stat.h>
#include <pthread.h>

#define PORT 8080
#define MAX_CLIENT 10
int valread;
pthread_t tid[1024];
int socket_id[1024];
pid_t child;

void printstr(char* str) {
    int i;
    for(i = 0; i < strlen(str); i++) {
        printf("%c%d ", str[i], str[i]);
    }
    printf("\n");
}

void registerUser(FILE* user_data, char* username, char* password) {
    size_t temp_size = 1024;
    char* temp = (char*) malloc(sizeof(char)*temp_size);
    bool unique = true;
    fseek(user_data, 0, SEEK_SET);
    while(fscanf(user_data, "%[^:]", temp) != EOF) {
        // printf("%s and %s 0\n", temp, username);
        // printstr(temp);
        // printstr(username);
        if(!strcmp(temp, username)) {
            unique = false;
            break;
        }
        // fscanf(user_data, "%s", temp);
        getline(&temp, &temp_size, user_data);
        memset(temp,0,strlen(temp));
        // printf("%s\n", temp);
    }
    if(unique) {
        int i;
        bool numValid = false, upperValid = false, lowerValid = false;
        for(i = 0; i < strlen(password); i++) {
            if(isdigit(password[i])) numValid = true;
            if(isupper(password[i])) upperValid = true;
            if(islower(password[i])) lowerValid = true;
        }
        if(strlen(password) > 5 && numValid && upperValid && lowerValid) {
            fprintf(user_data, "%s:%s\n", username, password);
            printf("New account registered\n");
            return;
        }
        printf("Failed to create new account: Password is too weak\n");
        printf("Password must have at least :\n");
        if(strlen(password) < 6) {
            printf("* 6 characters long\n");
        }
        if(!numValid) {
            printf("* A number\n");
        }
        if(!upperValid) {
            printf("* An uppercase character\n");
        }
        if(!lowerValid) {
            printf("* A lowercase character\n");
        }
    }
    else {
        printf("Failed to create new account: Username already used\n");
    }
}

bool loginUser(FILE* user_data, char* username, char* password) {
    size_t temp_size = 1024;
    char* temp = (char*) malloc(sizeof(char)*temp_size);
    // bool found = false;
    fseek(user_data, 0, SEEK_SET);
    while(fscanf(user_data, "%[^:]", temp) != EOF) {
        // printf("%s and %s 0\n", temp, username);
        // printstr(temp);
        // printstr(username);
        if(!strcmp(temp, username)) {
            // found = true;
            fgetc(user_data);
            fscanf(user_data, "%s", temp);
            if(!strcmp(temp, password)) {
                printf("Logged in successfully\n");
                return true;
            }
            else {
                printf("Password incorrect\n");
                return false;
            }
        }
        
        // fscanf(user_data, "%s", temp);
        getline(&temp, &temp_size, user_data);
        memset(temp,0,strlen(temp));
        // printf("%s\n", temp);
    }
    // if(!found)
        printf("User not found\n");
    return false;
}

void send_file(FILE *fp, int sockfd){
    int n;
    char data[1024] = {0};
 
    while(fgets(data, 1024, fp) != NULL) {
        if (send(sockfd, data, sizeof(data), 0) == -1) {
        perror("[-]Error in sending file.");
        exit(1);
        }
        bzero(data, 1024);
    }
    send(sockfd, "stop signal", sizeof("stop signal"), 0);
}

void write_file(int sockfd, char *filename){
    int n;
    FILE *fp;
    char buffer[1024];
    // printf("here1\n");
    fp = fopen(filename, "w");
    // bzero(buffer, 1024);
    // printf("here2\n");
    while (1) {
        // printf("here3\n");
        n = recv(sockfd, buffer, 1024, 0);
        if (n != 1024){
            break;
            return;
        }
        // printf("%d ", n);
        fprintf(fp, "%s", buffer);
        // printf("%s", buffer);
        bzero(buffer, 1024);
    }
    fclose(fp);
    return;
}

bool checkJudul(FILE* problem, char* judul) {
    size_t temp_size = 1024;
    char* temp = (char*) malloc(sizeof(char)*temp_size);
    bool unique = true;
    fseek(problem, 0, SEEK_SET);
    // printf("here\n");
    while(fscanf(problem, "%[^\t]", temp) != EOF) {
        // printf("%s and %s\n", temp, judul);
        // printstr(temp);
        // printstr(username);
        if(!strcmp(temp, judul)) {
            unique = false;
            break;
        }
        // fscanf(user_data, "%s", temp);
        getline(&temp, &temp_size, problem);
        memset(temp,0,strlen(temp));
        // printf("%s\n", temp);
    }
    return unique;
}

void addProblem(int new_socket, FILE* problem, char* username) {
    printf("Client requests to add problem\n");
    char temp[1024];
    // printf("Judul problem :\n");
    strcpy(temp, "Judul problem :");
    send(new_socket, temp, strlen(temp), 1024);
    char judul[1024] = {0};
    valread = read(new_socket, judul, 1024);
    printf("%s\n", judul);
    if(!checkJudul(problem, judul)) {
        strcpy(temp, "false");
        send(new_socket, temp, strlen(temp), 1024);
        return;
    }
    strcpy(temp, "true");
    send(new_socket, temp, strlen(temp), 1024);

    fprintf(problem,"%s\t%s\n", judul, username);
    mkdir(judul, 0777);
    printf("chdir %d\n", chdir(judul));

    char filepath[1048];
    printf("Filepath description.txt :\n");
    // sprintf(filepath, "%s/description.txt", judul);
    write_file(new_socket, "description.txt");

    printf("Filepath input.txt :\n");
    // sprintf(filepath, "%s/input.txt", judul);
    write_file(new_socket, "input.txt");

    printf("Filepath output.txt :\n");
    // sprintf(filepath, "%s/output.txt", judul);
    write_file(new_socket, "output.txt");
    chdir("..");
}

void seeProblem(int new_socket, FILE* problem) {
    printf("Client requests to see problem\n");
    size_t size = 1024;
    char* judul = (char*) malloc(sizeof(char)*size);
    char* author = (char*) malloc(sizeof(char)*size);
    char* temp = (char*) malloc(sizeof(char)*size);
    fseek(problem, 0, SEEK_SET);
    // printf("here\n");
    while(fscanf(problem, "%[^\t]", judul) != EOF) {
        // printf("%s and %s\n", temp, judul);
        // printstr(temp);
        // printstr(username);
        // fscanf(user_data, "%s", temp);
        fgetc(problem);
        fscanf(problem, "%[^\n]", author);
        
        sprintf(temp, "%s by %s\n", judul, author);
        send(new_socket, temp, strlen(temp), 1024);

        getline(&temp, &size, problem);
        memset(temp,0,strlen(temp));
        // printf("%s\n", temp);
    }
}

void downloadProblem(int new_socket, FILE* problem) {
    printf("Client requests to download problem\n");
    char temp[1024];
    strcpy(temp, "-= Download Problem =-");
    send(new_socket, temp, strlen(temp), 1024);
    char judul[1024] = {0};
    memset(judul,0,strlen(judul));
    valread = read(new_socket, judul, 1024);
    printf("%s\n", judul);
    if(checkJudul(problem, judul)) {
        strcpy(temp, "false");
        send(new_socket, temp, strlen(temp), 1024);
        return;
    }
    strcpy(temp, "true");
    send(new_socket, temp, strlen(temp), 1024);
    printf("chdir %d\n", chdir(judul));
    sleep(1);
    FILE* fp = fopen("description.txt", "r");
    if(fp == NULL)
        printf("File not found");
    send_file(fp, new_socket);
    fclose(fp);
    sleep(1);
    fp = fopen("input.txt", "r");
    send_file(fp, new_socket);
    fclose(fp);
    chdir("..");
    printf("Client downloaded problem successfully\n");
}

bool compareFiles(FILE *file1, FILE *file2){
    char ch1 = getc(file1);
    char ch2 = getc(file2);
    int error = 0, pos = 0, line = 1;
    bool equal = true;
    while (ch1 != EOF && ch2 != EOF){
        pos++;
        if (ch1 == '\n' && ch2 == '\n'){
            line++;
            pos = 0;
        }
        if (ch1 != ch2){
            equal = false;
            break;
        }
        ch1 = getc(file1);
        ch2 = getc(file2);
    }
    return equal;
}

void submitProblem(int new_socket, FILE* problem) {
    printf("Client requests to submit problem solution\n");
    char temp[1024];
    strcpy(temp, "-= Submit Problem Solution =-");
    send(new_socket, temp, strlen(temp), 1024);
    char judul[1024] = {0};
    memset(judul,0,strlen(judul));
    valread = read(new_socket, judul, 1024);
    printf("%s\n", judul);
    if(checkJudul(problem, judul)) {
        strcpy(temp, "false");
        send(new_socket, temp, strlen(temp), 1024);
        return;
    }
    strcpy(temp, "true");
    send(new_socket, temp, strlen(temp), 1024);
    printf("chdir %d\n", chdir(judul));
    write_file(new_socket, "temp.txt");
    FILE* submit = fopen("temp.txt", "r");
    FILE* output = fopen("output.txt", "r");
    if(compareFiles(submit, output))
        strcpy(temp, "Result: AC");
    else
        strcpy(temp, "Result: WA");
    send(new_socket, temp, strlen(temp), 1024);
    fclose(submit);
    fclose(output);
    remove("temp.txt");
    chdir("..");
    printf("Client submitted successfully\n");
}

void *handleClient(void *args) {
    int new_socket = *(int *)args;
    char buffer[1024] = {0};
    FILE* user_data = fopen("users.txt", "a+");
    FILE* problem = fopen("problems.tsv", "a+");
    // printf("here\n");
    char* welcome_msg = "-= Welcome to Bluemary's Online Judge =-\n";
    send(new_socket, welcome_msg, strlen(welcome_msg), 1024);
    // valread = read(new_socket, buffer, 1024);
    // printf("%s\n", buffer);
    char* home_msg = "Choose action:\n* register\n* login\n* exit";
    send(new_socket, home_msg, strlen(welcome_msg), 1024);
    while(read(new_socket, buffer, 1024) && strcmp(buffer, "exit")) {
        
        // printf("%s\n", buffer);
        if(!strcmp(buffer, "register")) {
            char* temp = "-= Register =-\nUsername : ";
            send(new_socket, temp, strlen(temp), 1024);

            char username[1024];
            memset(username,0,strlen(username));
            valread = read(new_socket, username, 1024);
            fflush(stdin);

            char* temp2 = "Password : ";
            send(new_socket, temp2, strlen(temp2), 1024);
            char password[1024];
            memset(password,0,strlen(password));
            valread = read(new_socket, password, 1024);
            fflush(stdin);
            registerUser(user_data, username, password);
            // fprintf(user_data, "%s:%s\n", username, password);
            fclose(user_data);
            user_data = fopen("users.txt", "a+");
            // strcpy(password, buffer);
            // char* temp3 = "Retype Password\t: ";
            // send(new_socket, temp3, strlen(temp3), 1024);
            // char passwordr[1024];
            // valread = read(new_socket, passwordr, 1024);
            // printf("un:%s\nps:%s\nrps:%s", username, password, passwordr);
        }
        else if(!strcmp(buffer, "login")) {
            char temp[1024] = "-= Login =-\nUsername : ";
            send(new_socket, temp, strlen(temp), 1024);

            char username[1000];
            memset(username,0,strlen(username));
            valread = read(new_socket, username, 1024);
            fflush(stdin);

            strcpy(temp, "Password : ");
            send(new_socket, temp, strlen(temp), 1024);
            char password[1024];
            memset(password,0,strlen(password));
            valread = read(new_socket, password, 1024);
            fflush(stdin);
            
            if(loginUser(user_data, username, password)) {
                sprintf(temp, "Welcome back, %s!\n", username);
                send(new_socket, temp, strlen(temp), 1024);
                strcpy(temp, "Choose action:\n* add\n* see\n* download\n* submit\n");
                send(new_socket, temp, strlen(temp), 1024);
                while(read(new_socket, buffer, 1024) && strcmp(buffer, "logout")) {
                    printf("%s\n", buffer);
                    if(!strcmp(buffer, "add")) {
                        addProblem(new_socket, problem, username);
                        fclose(problem);
                        problem = fopen("problems.tsv", "a+");
                    }
                    else if(!strcmp(buffer, "see")) {
                        seeProblem(new_socket, problem);
                    }
                    else if(!strcmp(buffer, "download")) {
                        downloadProblem(new_socket, problem);
                    }
                    else if(!strcmp(buffer, "submit")) {
                        submitProblem(new_socket, problem);
                    }
                    memset(buffer,0,strlen(buffer));
                    send(new_socket, temp, strlen(temp), 1024);
                }
                printf("Logged out\n");
            }

        }
        memset(buffer,0,strlen(buffer));
        send(new_socket, home_msg, strlen(welcome_msg), 1024);
        // valread = read(new_socket, buffer, 1024);
        // printf("%s\n", buffer);
    }
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    int counter = 0, err;
    while(1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        socket_id[counter] = new_socket;
        printf("A new client requests to connect...\n");
        char* temp = "Waiting to connect to server...\n";
        send(new_socket, temp, strlen(temp), 1024);
        if(counter) pthread_join(tid[counter-1],NULL);
        err = pthread_create(&(tid[counter++]), NULL, &handleClient, &new_socket);
        // if(err!=0) //cek error
		// 	printf("\n can't create thread : [%s]",strerror(err));
		// else
		// 	printf("\n create thread success\n");
    }

    return 0;
}

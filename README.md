# soal-shift-sisop-modul-3-B10-2022

# Kelompok B10 Sistem Operasi | Praktikum Modul 3
    Elthan Ramanda B           <-->  5025201092
    Bimantara Tito Wahyudi     <-->  5025201227
    Frederick Wijayadi Susilo  <-->  5025201111

## Source Soal Shift 3
Soal dapat diakses <a href="https://docs.google.com/document/d/1UP9p367kYnpwJHQvxtHUzIv4srfb2P9BqhGgaLFVkaM/edit?usp=sharing" target="_blank">disini</a>
## Penyelesaian Soal Shift 3

### Soal 1
Pada soal 1, Novak menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna dan mendownload berbagai informasi yang ada dari situs tersebut. Novak menemukan sebuah file dengan tulisan yang tidak jelas yang merupakan kode berformat base64. Novak meminta bantuan untuk membuatkan program untuk memecahkan kode-kode di dalam file yang tersimpan di drive dengan cara decoding dengan base 64 dengan menggunakan thread.

<details>
  <summary markdown="span">Tugas 1a</summary>

    Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file
    zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread. 
</details>

- Source Code

![1a.1](https://drive.google.com/uc?export=view&id=1a6nkYo-yFrcUIYEcUQA20KpxmXiDlKBo)

![1a.2](https://drive.google.com/uc?export=view&id=1YRzHonYQntQ1CojtI7m7NLReLoqFOknj)

![1a.3](https://drive.google.com/uc?export=view&id=1dlFsnVejEk2od-sXraqdJybS8ZqsIe-7)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mendownload dua file zip dan meng-unzip file zip tersebut pada dua folder berbeda secara bersamaan dengan thread.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, download dua file zip dan meng-unzip file zip dilakukan dengan pembuatan function `downloadFile()`. Function `downloadFile()` tersebut akan dijalankan oleh sebuah thread pada fungsi main dengan argumen NULL untuk masing-masing thread (terdapat 2 thread agar 2 file dapat didownload dan diunzip bersama-sama) dan kedua thread nantinya akan dijoinkan.

1. Pada function `downloadFile()` akan digunakan `pthread_t cur = pthread_self()` untuk mendapatkan id dari thread yang sedang dijalankan. Kemudian akan dibandingkan untuk masing-masing thread dengan id nya menggunakan `pthread_equal(cur, threads[i])`, jika merupakan thread pertama akan mendownload dan meng-unzip file `quote`, jika merupakan thread yang kedua akan mendownload dan meng-unzip file `music`.
2. Pada thread pertama, akan dibuat child process yang pertama-tama akan menjalankan command pembuatan folder dengan `execv("/bin/mkdir", cmd)` dengan isi dari `cmd` sendiri adalah `{"mkdir", "-p", "quote", NULL}`. Setelah itu, untuk child process berikutnya akan menjalankan command `execv("/bin/wget", cmd)` dengan `cmd` berisi `{"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download","-O", "quote.zip", NULL}` untuk mendownload file dan command `execv("/bin/unzip", cmd)` dengan `cmd` berisi `{"unzip", "-q", "quote.zip", "-d", "./quote", NULL}` untuk meng-unzip file.
3. Pada thread kedua, semua command yang dijalankan sama dengan thread pertama hanya saja `quote` diganti dengan `music` dan link download untuk file music adalah `https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download`.
4. Setelah itu, kedua thread akan dijoinkan sehingga dapat berjalan bersama-sama.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1b</summary>

    Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder
    (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt
    Masing-masing kalimat dipisahkan dengan newline/enter.
</details>

- Source Code

![1b.1](https://drive.google.com/uc?export=view&id=1zx6J0ktAULCw2gQNCfzwrIkjijPcoT5q)
Merupakan function untuk melakukan decode file `.txt` dengan base64 yang merupakan convert dari base64 format ke ASCII string format dari referensi <a href="https://github.com/elzoughby/Base64" target="_blank">berikut</a>.

![1b.2](https://drive.google.com/uc?export=view&id=1nqpjwk6UMNp2USF6sv1EmreySlPUODZL)

![1b.3](https://drive.google.com/uc?export=view&id=10-mSZ4jGeA9xVFc-SczoJZZ2QuIhifvP)

![1b.4](https://drive.google.com/uc?export=view&id=1LhYph7fRiyuqGnaLV9ZPM5pO35Qpuxzn)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mendecode file .txt dengan base64 dan memasukkan hasilnya dalam satu file .txt pada masing-masing folder dengan thread.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, decode file `.txt` dengan base64 dilakukan dengan function `decode()`. Function `decodeFile()` merupakan function yang akan dijalankan oleh sebuah thread pada fungsi main dengan argumen NULL untuk masing-masing thread (terdapat 2 thread agar 2 folder dapat didecode semua filenya bersama-sama) dan kedua thread nantinya akan dijoinkan.

1. Pada function `decodeFile()` akan digunakan `pthread_t id = pthread_self()` untuk mendapatkan id dari thread yang sedang dijalankan. Kemudian akan dibandingkan untuk masing-masing thread dengan id nya menggunakan `pthread_equal(cur, threads[i])`, jika merupakan thread pertama akan mendecode semua file `.txt` pada folder `quote`, jika merupakan thread yang kedua akan mendecode semua file `.txt` pada folder `music`.
2. Pada thread pertama, dibuat variabel `folder` bertipe `DIR` dan `de` bertipe `struct dirent`, kemudian membuka folder `quote` sesuai path pada variabel `folder` dan membaca isi semua folder `quote` dimana setiap file yang ada pada folder tersebut akan diassign di variabel `de` dengan mengabaikan file `.` dan `..`.
3. Selanjutnya, dibuat pada iterasi pembacaan file tersebut, dibuat variabel `file` yang merupakan path file yang dibaca nantinya, dan dibuat pula variabel `f` bertipe `FILE` yang akan berisi file yang dibaca, kemudia dibaca setiap line pada file tersebut menggunakan `fgets(str, sizeof(str), f)` dimana string `str` merupakan hasil yang didapat pada line tertentu pada file.
4. Setelah itu masih pada bagian yang sama, string `str` akan didecode dengan function `decode()` dan kemudian hasil decode yang didapat akan ditulis pada file dengan argumen `a` atau `append` dengan path berupa variabel `loc` yaitu path menuju ke `quote.txt` dengan newline pada setiap barisnya.
5. Pada thread kedua, semua command yang dijalankan sama dengan thread pertama (langkah 2-4) hanya saja `quote` diganti dengan `music` baik itu pada path yang dibaca, path yang dituju, ataupun hasil file `.txt`.
6. Setelah itu, kedua thread akan dijoinkan sehingga dapat berjalan bersama-sama.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1c</summary>

    Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
</details>

- Source Code

![1c](https://drive.google.com/uc?export=view&id=1UNiIep6ey4zxJy2gHb5-1c-xgZs-pIUH)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Memindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, pemindahan file dilakukan dengan function `moveFile()`. Function `moveFile()` akan dijalankan pada fungsi main.

1. Pada function `moveFile()`, akan dibuat child process yang pertama-tama akan membuat folder `hasil` dengan menjalankan command pembuatan folder dengan `execv("/bin/mkdir", cmd)` dengan isi dari `cmd` sendiri adalah `{"mkdir", "-p", "hasil", NULL}`.
2. Setelah itu, untuk child process berikutnya akan menjalankan command `execv("/bin/mv", cmd)` dengan `cmd` berisi `{"mv", "./quote/quote.txt", "./hasil", NULL}` untuk memindahkan file `quote.txt` ke folder `hasil` dan dengan `cmd` berisi `{"mv", "./music/music.txt", "./hasil", NULL}` untuk memindahkan file `music.txt` ke folder `hasil`.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1d</summary>

    Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
</details>

- Source Code

![1d](https://drive.google.com/uc?export=view&id=1Fh-xHw5w97jkP9TxKFCh5EYKIaFenRLJ)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Meng-zip folder hasil menjadi hasil.zip dengan password 'mihinomenest[Nama user]', pada kelompok kami menggunakan password 'mihinomenestfrederick'.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, meng-zip file dilakukan dengan function `zipFile()`. Function `zipFile()` akan dijalankan pada fungsi main. Pada function `zipFile()`, akan dibuat child process yang menjalankan command `execv("/bin/zip", cmd)` dengan isi dari `cmd` sendiri adalah `{"zip", "-P", "mihinomenestfrederick", "-r", "./hasil.zip", "-q", "./hasil", NULL}` dengan `-P` merupakan argumen untuk memberikan password.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 1e</summary>

    Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang
    bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
</details>

- Source Code

![1e.1](https://drive.google.com/uc?export=view&id=1YAGMSNcUmr4-h3CYwfxaRyYzaYJQvciE)

![1e.2](https://drive.google.com/uc?export=view&id=1UHwwyXTa1oA7iMu9F2JtUho6XNlKhsCL)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Meng-unzip file hasil.zip dan membuat file no.txt yang berisi 'No' pada saat yang bersamaan, lalu meng-zip kedua file tersebut menjadi hasil.zip dengan password yang sama.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, meng-unzip file dan membuat file `no.txt` dilakukan dengan pembuatan function `additionFile()`. Function `additionFile()` tersebut akan dijalankan oleh sebuah thread pada fungsi main dengan argumen NULL untuk masing-masing thread (terdapat 2 thread agar kedua command dapat dijalankan bersama-sama) dan kedua thread nantinya akan dijoinkan.

1. Pada function `additionFile()` akan digunakan `pthread_t cur = pthread_self()` untuk mendapatkan id dari thread yang sedang dijalankan. Kemudian akan dibandingkan untuk masing-masing thread dengan id nya menggunakan `pthread_equal(cur, threads[i])`, jika merupakan thread pertama akan meng-unzip file `hasil.zip`, jika merupakan thread yang kedua akan membuat file `no.txt`.
2. Pada thread pertama, akan dibuat child process yang menjalankan command `execv("/bin/unzip", cmd)` dengan isi dari `cmd` sendiri adalah `{"unzip", "-q", "hasil.zip", "-d", "./hasil", NULL}`.
3. Pada thread kedua, akan dilakukan pembuatan file `no.txt` pada folder `hasil` dengan argumen `w` atau `write` yang kemudian akan dituliskan `No` pada file tersebut.
4. Setelah itu, kedua thread akan dijoinkan sehingga dapat berjalan bersama-sama.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

#### Output Soal 1

1. Proses dan hasil compile soal 1a.

![out1a](https://drive.google.com/uc?export=view&id=1DNj4fkYW5wRYkg943Gs_eDMiBbJXWTz_)

2. Proses dan hasil compile soal 1b.

![out1b.1](https://drive.google.com/uc?export=view&id=1TzXlVMd2D4bqkv-Z5Et3FaHkosoVR4Fx)

3. Isi file `quote.txt`.

![out1b.2](https://drive.google.com/uc?export=view&id=1UondVBEORuWPKo5l4IjSIUJ1Cvrewe-j)

4. Isi file `music.txt`.

![out1b.3](https://drive.google.com/uc?export=view&id=1Y4sXIkb6Xx2AD79zG8QfhsEnfhWXKuZD)

5. Proses dan hasil compile soal 1c.

![out1c](https://drive.google.com/uc?export=view&id=1HO-hSysCcmFmHdl9BKbXyEcL24dkNrdy)

6. Proses dan hasil compile soal 1d.

![out1d.1](https://drive.google.com/uc?export=view&id=1nPQE0lNgumJzslTzZkBfaEJTin5iJW_w)

7. Hasil zip folder `hasil`.

![out1d.2](https://drive.google.com/uc?export=view&id=1tuGuNf_j1NqLaLkLc19JYmBampo-Mu4W)

8. Bukti menggunakan password pada `hasil.zip`.

![out1d.3](https://drive.google.com/uc?export=view&id=1k_S6aET9qtotKcrbbTbmWNwCh__ZWA-_)

9. Jika password yang dimasukkan salah.

![out1d.4](https://drive.google.com/uc?export=view&id=141eTjtLAcxMMyKntzkEWDz2LfYoVrsvd)

10. Proses dan hasil compile soal 1e.

![out1d.1](https://drive.google.com/uc?export=view&id=1y8nFcUQNrmTphpbmqcE-ONAJFWmgimJ1)

11. Hasil zip folder `hasil`.

![out1d.2](https://drive.google.com/uc?export=view&id=1MobtQfujWLHbHjUyqz9l4911ir2NIfv8)

12. Bukti menggunakan password pada `hasil.zip`.

![out1d.3](https://drive.google.com/uc?export=view&id=1TH90pXS-8nwEZTnxYddLHUpQ_s7CdKsO)

13. Jika password yang dimasukkan salah.

![out1d.4](https://drive.google.com/uc?export=view&id=18VTWHF7NW7_HfyzyUdAv4gOb4eqzmQod)

### Soal 2

Pada soal 2 kami diminta untuk membuat online judge simpel dengan sistem client-server menggunakan socket programming

#### Source Code Socket Server

![2.1](https://drive.google.com/uc?export=view&id=1EwbkoP6E_qdaXM_RL9Gq7RBMD_ubaHox)

#### Source Code Socket Client

![2.2](https://drive.google.com/uc?export=view&id=19OcbNKWXGIka-V2UNXT5c18UsfXonGl3)

#### Source Code Server Soal 2.a dan 2.b

![2.3](https://drive.google.com/uc?export=view&id=1DqCKqbhYzhH9tIyl6RPY6lVmIkUA1ziF)

![2.4](https://drive.google.com/uc?export=view&id=1gg2ErneDv5Pgbvt4OSd984XPl4AI7qNU)

#### Source Code Client Soal 2.a dan 2.b

![2.5](https://drive.google.com/uc?export=view&id=1hKZ_bqz590hmZaPkUCPhVmYpH2DB2LxD)

#### Cara Pengerjaan

Tujuan: Memberikan pilihan `login` dan `register` kepada client yang terkoneksi dengan server dan menyimpan data user dan problem di server

1. Dalam fungsi `handleClient`, file `users.txt` dan `problem.tsv` dibuka di awal
2. Client menggunakan multithreading dengan fungsi `readandsend` untuk membaca sekaligus mengirim pesan kepada server
3. Server mengirim pilihan kepada client yang baru terkoneksi dengan server
4. Apabila client memilih register, maka akan diminta username dan password yang kemudian di-cek validitasnya sekaligus menyimpan ke dalam database menggunakan fungsi `registerUser`
5. Apabila client memilih login, maka akan diminta username dan password juga yang kemudian di-cek menggunakan fungsi `loginUser`
6. Apabila client berhasil login, maka client dapat mengakses berbagai fitur dengan mengirim command kepada server

#### Source Code Server Soal 2.c

![2.6](https://drive.google.com/uc?export=view&id=1rH0dgleKM2ZpYwQMZxDiQgJQOpN97Z_r)

![2.9](https://drive.google.com/uc?export=view&id=1BMRE9kBnnVWBCy05av1w4OflP442inFO)

#### Source Code Client Soal 2.c

![2.7](https://drive.google.com/uc?export=view&id=1bdXiCrLRbuCVkn8CGymsERGuybgIt3Es)

#### Cara Pengerjaan

Tujuan: Client dapat menambah soal baru pada sistem

1. Client mengirim command `add` kepada server kemudian server dan client bersiap untuk melakukan penambahan soal dengan fungsi `addProblem`
2. Client memasukkan judul dilanjut dengan directory `description.txt`, `input.txt`, dan `output.txt` yang akan digunakan program client untuk mengirim file kepada server dengan menggunakan fungsi `send_file`
3. Server menerima file yang dikirim oleh client dengan fungsi `write_file`

#### Source Code Server Soal 2.d

![2.8](https://drive.google.com/uc?export=view&id=1v95QZ4_ahwvPO9yS41QeDSRBSoUc6GkR)

#### Cara Pengerjaan

Tujuan: Client dapat melihat semua soal yang tersedia

1. Server membaca semua problem dan authornya dalam file `problem.tsv` dan kemudian mengirimkan list problem kepada client
2. Client menerima otomatis dengan multithreading sebelumnya

#### Source Code Server Soal 2.e

![2.10](https://drive.google.com/uc?export=view&id=17IPKsjwbjKDX0D2BqKDGyFPhU99UTm5S)

#### Source Code Client Soal 2.e

![2.11](https://drive.google.com/uc?export=view&id=10rl3s4eeGmqkij8F0TVhm7WtHHHyyNP6)

#### Cara Pengerjaan

Tujuan: Client dapat mendownload soal yang tersedia

1. Client mengirim judul problem yang akan di-download kepada server
2. Server melakukan pengecekan judul, kemudian server mengirim file `description.txt` dan `input.txt` problem sesuai judul yang diminta menggunakan fungsi `send_file`
3. Client menerima file dari server dengan fungsi `write_file`

#### Source Code Server Soal 2.f

![2.12](https://drive.google.com/uc?export=view&id=1QJ3yTfCMTZDOP1TJDrovcn1y2qYwMUer)

#### Source Code Client Soal 2.f

![2.13](https://drive.google.com/uc?export=view&id=1buB75xgCEGE2fDBbkK7I78THuH86JcH0)

#### Cara Pengerjaan

Tujuan: Client dapat submit solusi problem dan mendapat hasilnya

1. Client mengirim judul problem yang akan di-submit kepada server
2. Server melakukan pengecekan judul, kemudian menerima file dari client yang berupa `output.txt` ke dalam file sementara `temp.txt`
3. Server melakukan komparasi antara file `output.txt` dari problem di server dan file `temp.txt` yang merupakan solusi dari client
4. Server mengirimkan hasil komparasi kepada client

#### Source Code Server Soal 2.g

![2.14](https://drive.google.com/uc?export=view&id=14MAb05m-N8-hA7Pp5ovPrq-fO0AtY_UY)

#### Cara Pengerjaan

Tujuan: Server dapat menangani multiple-connection

1. Digunakan multithreading pada server untuk menangani 2 atau lebih client yang mencoba masuk
2. Ketika ada client yang masuk, maka id socketnya akan disimpan dan akan dicek apakah ada client yang masuk sebelumnya
3. Apabila ada, maka thread client sebelumnya akan di-join sehingga client baru harus menunggu terlebih dahulu sampai client sebelumnya selesai barulah thread baru untuk client baru akan dibentuk, dst

#### Kendala

Tidak ditemukan kendala saat pengerjaan soal 2

#### Output

1. Directory server 

![2.15](https://drive.google.com/uc?export=view&id=13s33_DffFgBWYFLExYxEcA_G_Xq4b7ae)

2. Directory client

![2.16](https://drive.google.com/uc?export=view&id=18GStrpPWZdcJzmw-3llh2D0_sw1rllIc)

3. Isi file `users.txt`

![2.17](https://drive.google.com/uc?export=view&id=1dJBcnCp9lljmgqKM96kaHFZvRVPqpo4b)

4. Isi file `problem.tsv`

![2.18](https://drive.google.com/uc?export=view&id=19YTQp0ezpDJyqmtsZkqdtn4c073AaSlH)

5. Percobaan `register` dan `login` oleh client

![2.19](https://drive.google.com/uc?export=view&id=1tmfpBlsthfreimL3Z2F9t2Lg66UDW-gq)

6. Percobaan `add` dan `see` oleh client

![2.20](https://drive.google.com/uc?export=view&id=1x6pASxkA2VnHkzm2E8OPZEy0StLV2UcT)

7. Percobaan `download` dan `submit` oleh client

![2.21](https://drive.google.com/uc?export=view&id=1yNDwkSZpO1tdhYcUjpLERiMTkUIxos-k)

8. Directory server setelah eksekusi

![2.23](https://drive.google.com/uc?export=view&id=1Dvf_ABDuJGgCnDXLvRBMZ49nQs4xhnKv)

9. Directory client setelah eksekusi

![2.22](https://drive.google.com/uc?export=view&id=1_SrhgoeJMus4Wh3RJwqA1ATj_Way_Tf5)

10. Isi file `users.txt` setelah eksekusi

![2.24](https://drive.google.com/uc?export=view&id=1NjbfdZ_CgIb2fr_mKFV66-0fJpI_5uFT)

11. Isi file `problem.tsv` setelah eksekusi

![2.25](https://drive.google.com/uc?export=view&id=1N0Wz0rVptXFjDpk76JLWbXIaxASkbMgo)

### Soal 3
Pada soal 3, Disini Nami diminta untuk mengkategorikan harta karun yang ia punya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Sesudah ia kategorikan, harta karun ini akan dia kirim menuju kampung halamannya.

<details>
  <summary markdown="span">Tugas 3a</summary>
  
    Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif
</details>

- Source Code

![unzip](https://drive.google.com/uc?export=view&id=1KYK-r12pAbuA9TiVAwG79tQWBoyHLVWO)

![soal3a.1](https://drive.google.com/uc?export=view&id=1pO7OwWCARCUY4P76sAbMOr8WvYa8bQJr)

![soal3a.2](https://drive.google.com/uc?export=view&id=1FI_4rvyXudCu2Dh8HJSwHJ5I9OgfB1M4)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Meng-Unzip file hartakarun.zip melalui terminal yang ditaruh pada directory home/user/shift3/ yang kemudian mengganti working directory pada home/user/shift3/hartakarun apabila sudah di unzip .**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code diatas diperlihatkan kita meng-unzip langsung melalui terminal karena permintaan soal yang kemudian mengganti working directory dengan `chdir("/home/rado/shift3/hartakarun")` (dalam hal ini rado adalah user).

<details>
  <summary markdown="span">Tugas 3b</summary>
  
    Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

</details>

- Source Code

![soal3b.1](https://drive.google.com/uc?export=view&id=1pO7OwWCARCUY4P76sAbMOr8WvYa8bQJr)

![soal3b.2](https://drive.google.com/uc?export=view&id=101Af_Wf3pKRW_QRD_IHy0n-iF3CJexVi)

![soal3b.3](https://drive.google.com/uc?export=view&id=1Fb_WWKpOdiklPPmQtdU6nZbvsNYYUEjB)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Memulai mengkategorikan file yang sudah di unzip dengan mengikuti beberapa peraturan yaitu terdapat file yang di hidden serta melakukan rekursi saat pengkategorian.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code diatas diperlihatkan fungsi `getFolderName`, pada fungsi ini kita akan mengambil filename saja dengan menggunakan command `strchr(str, '/')` sehingga pada format path yang dituju hanya mengambil kalimat setelah `/` yang terakhir.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code selanjutnya diperlihatkan fungsi `getExtension`, pada fungsi ini kita akan mengeliminasi yang bukan extensi nya dengan menggunakan command `strchr(foccur, '.')` tetapi pada fungsi ini juga kita mengambil `.` yang pertama untuk diambil katanya sehingga tidak perlu dilakukan looping seperti fungsi `getFolderName` (karena dalam file terdapat file dengan ekstensi `.tar.gz`).

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code selanjutnya diperlihatkan fungsi `moveFile`, Pada fungsi ini kita melakukan store extensi file tersebut menggunakan fungsi `getExtension` ke variabel `extend` dan filename dengan fungsi `getFolderName` ke variabel `fileName`. Jika file tersebut merupakan Hidden file maka `myfolder` berisi `Hidden` dengan melakukan check `if (fileName[0] == '.')` pada awalan dari file name tersebut, sedangkan apabila file tersebut tidak memiliki extensi maka akan `myfolder` akan berisi `Unknown`. Dan untuk yang memiliki extensi serupa namun penulisannya menggunakan huruf yang berbeda (Ex : jpg dan JPG) akan dianggap huruf kecil melalui `tolower(extend[i]);` yang kemudian ditaruh ke `myFolder` karena perintah soal tidak case sensitive, selanjutnya kita menggunakan  `mkdir(myFolder, 0777);` untuk membuat directory yang namanya diambil dari `myfolder` tersebut.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code selanjutnya diperlihatkan fungsi `moveFileUtil`, Pada fungsi ini kita melakukan read pada source yang berisi `fileAsli` dan melakukan write pada file `destDir` yang baru. setelah proses selesai maka file source akan dihapus dengan command `remove(source);`

<details>
  <summary markdown="span">Tugas 3c</summary>
  
    Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

</details>

- Source Code

![soal3c](https://drive.google.com/uc?export=view&id=1aVNFBZGAOqrbKEYwchlpLMMNSssmUDCu)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan :Melakukan pengkategorian file yang dilakukan oleh 1 thread untuk 1 file dengan rekursif**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada fungsi `category`, pada fungsi ini kita melakukan scan berapa banyak file yang terdapat pada folder terpilih dengan command `n = scandir(path, &de, NULL, alphasort);`. Alphasort bertujuan untuk melakukan sort berdasarkan alfabet sehingga dapat menghindari kegagalan disaat ada namafile yang sama dengan extensi dari file lain yaitu dengan melakukan proses pada file tersebut dan masuk ke folder `Unknown` sehingga apabila ada file yang memiliki extensi yang sama dengan file sebelumnya, folder extensi dapat terbuat.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kemudian kita lakukan rekursi untuk membaca semua file pada semua folder hingga akarnya. Jika `n < 0` yang berarti sudah tidak ada file lagi pada folder tertentu, maka pembacaan pada folder tersebut sudah selesai. Jika tidak, maka akan dilakukan pembacaan semua file pada folder tersebut, sedangkan untuk folder akan dicari lagi lebih dalam. Selanjutnya  kita akan mengeliminasi `. , ..` yang terdapat pada saat melakukan scan dan pada variabel `src` kita mengisi dengan `path` dan menambah `/` serta filename yang sebelumnya kita store di struct dirent **de.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saat melakukan scan disini tidak membedakan antara direktori atau file sehingga kita melakukan `if` jika bukan direktori/folder maka kita lakukan command membuat thread yang menjalankan function `moveFile` dengan argumen `src` untuk melakukan pengkategorian. File yang berhasil dikategorikan akan meghasilkan `berhasil dikategorikan` dan apa bila gagal maka `gagal dikategorikan`. Setiap thread akan dijoinkan sehingga pada saat proses berlangsung tidak perlu menunggu file sebelumnya selesai diproses atau dengan kata lain dapat bekerja bersama-sama sekaligus.

<details>
  <summary markdown="span">Tugas 3d</summary>
  
    Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
</details>
<details>
  <summary markdown="span">Tugas 3e</summary>
  
    Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server.
</details>

- Source Code

![soal3de.1](https://drive.google.com/uc?export=view&id=1PYgpqvW6m5eAQ6eSOywi0Jo3TaASFcqU)

![soal3de.2](https://drive.google.com/uc?export=view&id=1J2_vhtSKvpDPVyMV2dPXh-L7KKPu5NXa)

![soal3de.3](https://drive.google.com/uc?export=view&id=1XfXvgOfSPHzXGjSJEG-IH5tsWLJpTwNh)

![soal3de.4](https://drive.google.com/uc?export=view&id=1Y_ueIm0N-zIItD6cxeL1zM_gxtBpFmOG)

![soal3de.5](https://drive.google.com/uc?export=view&id=1rVjO6XRvm1Z-6D4V1sftHoHHfA7aaN6h)

![soal3de.6](https://drive.google.com/uc?export=view&id=1r_ImdrzgLvruv5U8oFeBMcz97C1MQ53s)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan :Melakukan zip file hasil dari pengkategorian agar dapat disend melalui server**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada source code diatas terdapat beberapa fungsi, pada `client.c` terdapat fungsi `zip_file` dimana fungsi ini sendiri berfungsi untuk mengzip folder-folder yang sudah dikategorikan agar nantinya ada file `hartakarun.zip` yang akan dikirim.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada `int main` terdapat `filename` yang nantinya akan diisi `hartakarun.zip` dan `hostname` dimana akan diisi oleh ip address dan juga port disini kita menggunakan 8080.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya ada cmd dimana `if` jika kita (user) menginput command `send hartakarun.zip` maka akan dilanjut ke fungsi selanjutnya, namun apabila kita salah dalam input command maka program akan muncul `Wrong command` kemudian keluar dan harus di run lagi.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `if (argc > 1)` digunakan untuk memastikan apakah file yang akan dikirim adalah `hartakarun.zip`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `filestream = open(file_name, O_RDONLY);` digunakan untuk menampung file yang akan dikirim nantinya dalam mode `read only`/

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `proto = getprotobyname("tcp");` disini kita menggunakan socket `tcp`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `sock = socket(AF_INET, SOCK_STREAM, proto->p_proto);` disini untuk mendapatkan socket.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `host = gethostbyname(hostname);` disini untuk mendapatkan hostname.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `in_address = inet_addr(inet_ntoa(*(struct in_addr*)*(host->h_addr_list)));` disini digunakan untuk mendapatkan addressnya kemudian atribut atribut sebelumnya di assign ke `socket_address`

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `if (connect(sock, (struct sockaddr*)&socket_address, sizeof(socket_address)) == -1)` disini untuk menyambungkan client ke server.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya dilakukan perulangan untuk mendapatkan status dari pembacaan file untuk mengirimkan file sekaligus mememeriksa error pada saat read and write.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beralih ke `server.c`, pada filename masih sama dengan `client.c` dan port juga masih sama, lalu disini untuk `getprotobyname` dan `socket`
masih sama, bedanya ada pada penamaan socketnya.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya pada command `if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr, sizeof(enable_reuseaddr)) < 0)` disini untuk menetapkan opsi socket untuk melakukan bind.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kemudian atribut atribut sebelumnya di assign ke `server_address`, kemudian kita melakukan `bind` dan `listen` untuk server.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya terdapat `fprintf(stderr,"listening on port", port)` untuk memastikan apakah server sudah connect dengan port yang diinginkan.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kemudian dilakukan perulangan yang mendapatkan `size` dari client address, dan selanjutnya kita menunggu client untuk mengirimkan pesan.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesudah client mengirimkan pesan maka akan direspon oleh server dan pesannya diterima untuk di jalankan dengan syntax `filestream = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Selanjutnya dilakukan perulangan untuk mendapatkan status dari pembacaan file untuk menerima file yang dikirimkan sekaligus mememeriksa error pada saat read and write.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mengirim file .zip dari client ke server.


#### Output Soal 3

1. Hasil compile unzip menggunakan command `unzip -q hartakarun.zip -d /home/user/shift3` (dalam pengujian user yang digunakan adalah `rado`).

![Output 1](https://drive.google.com/uc?export=view&id=19J5tJVFcv3iIyxtnEJq53b2FUxIg1K0j)

2. Pengkategorian file berhasil menggunakan command `gcc -pthread -o -tes -modul33.c` dilanjutkan `./tes`.

![Output 2](https://drive.google.com/uc?export=view&id=1633g-gEoCXdc6ZYUs2CI7P9ViOJBtV19)

3. File yang sudah dimasukkan kedalam folder yang sudah diurutkan dan dikategorikan.

![Output 3](https://drive.google.com/uc?export=view&id=1twa7RtttOsRUGCc4CwNbNRBSyEQ3mSEX)

4. Folder hidden dan unknown (note untuk golder hidden seharusnya tampilannya kosong namun digunakan `ctrl + h` agar dapat melihat isi dari folder tersebut).

![hidden](https://drive.google.com/uc?export=view&id=1KKbvkm0sQzGzhe9dVZOqwAkEkGffOzED)

![unknown](https://drive.google.com/uc?export=view&id=1GQ662tV0oNwa9GleBANgohMbW0S62t0F)

5. Karena file sudah dikategorikan, selanjutnya kita akan input command `gcc -o server server.c` lalu dibuka diterminal dengan `./server` sehingga servernya aktif lalu dilanjut `gcc -o client client.c` kemudian `./client` sehingga kita dapat mengirim file dengan command `send hartakarun.zip`

![output 3de](https://drive.google.com/uc?export=view&id=1xe6IVXmMusDyq0VP3gT-6bdiQOzIJDlZ)

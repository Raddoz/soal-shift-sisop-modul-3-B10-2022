#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <dirent.h>
#include <string.h>

pthread_t threads[100000];
pid_t child_process, child_process2;
char res[100];

char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

void clearAll()
{
    char path[] = "./hasil", path1[] = "./quote", path2[] = "./music", path3[] = "./hasil.zip", path4[] = "./quote.zip", path5[] = "./music.zip";
    int status;
    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;

    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path1, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;

    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path2, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;

    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path3, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;

    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path4, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;

    if (child_process == fork())
    {
        char *cmd[] = {"rm", "-r", path5, NULL};
        execv("/bin/rm", cmd);
    }

    while ((wait(&status)) > 0)
        ;
}

void *downloadFile(void *arg)
{
    pthread_t cur = pthread_self();

    if (pthread_equal(cur, threads[0]))
    {
        int status;
        if (child_process == fork())
        {
            char *cmd[] = {"mkdir", "-p", "quote", NULL};
            execv("/bin/mkdir", cmd);
        }
        else
        {
            while ((wait(&status)) > 0)
                ;
            int status2;

            if (child_process2 == fork())
            {
                char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
                               "-O", "quote.zip", NULL};
                execv("/bin/wget", cmd);
            }
            while ((wait(&status2)) > 0)
                ;

            if (child_process2 == fork())
            {
                char *cmd[] = {"unzip", "-q", "quote.zip", "-d", "./quote", NULL};
                execv("/bin/unzip", cmd);
            }
            while ((wait(&status2)) > 0)
                ;
        }
    }
    else if (pthread_equal(cur, threads[1]))
    {
        pthread_join(threads[0], NULL);
        int status;
        if (child_process == fork())
        {
            char *cmd[] = {"mkdir", "-p", "music", NULL};
            execv("/bin/mkdir", cmd);
        }
        else
        {
            while ((wait(&status)) > 0)
                ;
            int status2;

            if (child_process2 == fork())
            {
                char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download",
                               "-O", "music.zip", NULL};
                execv("/bin/wget", cmd);
            }
            while ((wait(&status2)) > 0)
                ;

            if (child_process2 == fork())
            {
                char *cmd[] = {"unzip", "-q", "music.zip", "-d", "./music", NULL};
                execv("/bin/unzip", cmd);
            }
            while ((wait(&status2)) > 0)
                ;
        }
    }
    return NULL;
}

void decode(char *cipher)
{
    char buffer[4], result[100];
    int i = 0, ind = 0, count = 0, k;

    while (cipher[i] != '\0')
    {
        for (k = 0; k < 64 && base64_map[k] != cipher[i]; k++)
            ;
        buffer[count++] = k;
        if (count == 4)
        {
            result[ind++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                result[ind++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                result[ind++] = (buffer[2] << 6) + buffer[3];
            count = 0;
        }
        i++;
    }

    result[ind] = '\0';
    strcpy(res, result);
}

void *decodeFile(void *arg)
{
    pthread_t id = pthread_self();

    if (pthread_equal(id, threads[0]))
    {
        DIR *folder;
        struct dirent *de;
        char path[] = "./quote";

        folder = opendir(path);
        while ((de = readdir(folder)) != NULL)
        {
            if (strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
            {
                char str[150], loc[150], file[] = "./quote/";
                int cnt = 0;
                strcat(file, de->d_name);
                FILE *f = fopen(file, "r");
                if (f == NULL)
                {
                    printf("Cannot open file\n");
                    continue;
                }
                fgets(str, sizeof(str), f);
                fclose(f);
                decode(str);
                strcpy(loc, path);
                strcat(loc, "/quote.txt");
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "%s\n", res);
                fclose(wfile);
            }
        }
        closedir(folder);
    }
    else if (pthread_equal(id, threads[1]))
    {
        pthread_join(threads[0], NULL);
        DIR *folder;
        struct dirent *de;
        char path[] = "./music";

        folder = opendir(path);
        while ((de = readdir(folder)) != NULL)
        {
            if (strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
            {
                char str[100], loc[150], file[] = "./music/";
                int cnt = 0;
                strcat(file, de->d_name);
                FILE *f;
                f = fopen(file, "r");
                if (f == NULL)
                {
                    printf("Cannot open file\n");
                    continue;
                }
                fgets(str, sizeof(str), f);
                fclose(f);
                decode(str);
                strcpy(loc, path);
                strcat(loc, "/music.txt");
                FILE *wfile = fopen(loc, "a");
                fprintf(wfile, "%s\n", res);
                fclose(wfile);
            }
        }
        closedir(folder);
    }
    return NULL;
}

void moveFile()
{
    int status;
    if (child_process == fork())
    {
        char *cmd[] = {"mkdir", "-p", "hasil", NULL};
        execv("/bin/mkdir", cmd);
    }
    else
    {
        while ((wait(&status)) > 0)
            ;
        int status2;
        if (child_process2 == fork())
        {
            char *cmd[] = {"mv", "./quote/quote.txt", "./hasil", NULL};
            execv("/bin/mv", cmd);
        }
        while ((wait(&status2)) > 0)
            ;

        if (child_process2 == fork())
        {
            char *cmd[] = {"mv", "./music/music.txt", "./hasil", NULL};
            execv("/bin/mv", cmd);
        }
        while ((wait(&status2)) > 0)
            ;
    }
}

void zipFile()
{
    int status;
    child_process = fork();
    if (child_process == 0)
    {
        char *cmd[] = {"zip", "-P", "mihinomenestfrederick", "-r", "./hasil.zip", "-q", "./hasil", NULL};
        execv("/bin/zip", cmd);
    }
    while ((wait(&status)) > 0)
        ;
}

void *additionFile(void *arg)
{
    pthread_t cur = pthread_self();

    if (pthread_equal(cur, threads[0]))
    {
        int status;
        if (child_process == fork())
        {
            char *cmd[] = {"unzip", "-q", "hasil.zip", "-d", "./hasil", NULL};
            execv("/bin/unzip", cmd);
        }

        while ((wait(&status)) > 0)
            ;
    }
    else if (pthread_equal(cur, threads[1]))
    {
        pthread_join(threads[0], NULL);
        FILE *wfile = fopen("./hasil/no.txt", "w");
        fprintf(wfile, "No");
        fclose(wfile);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    clearAll();
    int num = 2, d;
    for (int i = 0; i < num; i++)
    {
        d = pthread_create(&(threads[i]), NULL, &downloadFile, NULL);
        if (d)
            printf("File %d gagal di-unzip\n", i + 1);
        else
            printf("File %d berhasil di-unzip\n", i + 1);
    }
    pthread_join(threads[1], NULL);

    for (int i = 0; i < num; i++)
    {
        d = pthread_create(&(threads[i]), NULL, &decodeFile, NULL);
        if (d)
            printf("File %d gagal di-decode\n", i + 1);
        else
            printf("File %d berhasil di-decode\n", i + 1);
    }
    pthread_join(threads[1], NULL);

    moveFile();
    zipFile();
    for (int i = 0; i < num; i++)
    {
        d = pthread_create(&(threads[i]), NULL, &additionFile, NULL);
        if (d)
            printf("File %d gagal ditambahkan\n", i + 1);
        else
            printf("File %d berhasil ditambahkan\n", i + 1);
    }
    pthread_join(threads[1], NULL);

    zipFile();
    exit(0);
    return 0;
}